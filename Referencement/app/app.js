var app = angular.module('app', ['ngRoute', "ngResource"]);

app.config(function($routeProvider,$httpProvider) {

	$routeProvider.when('/', {
		templateUrl: "view/accueil.html"
	})
	.when('/refs/:refId', {
		templateUrl: "view/details.html"
	})
	.otherwise({
		redirectTo: '/'
	});
});

app.controller('refCtrl', function($scope,$http,$route,$routeParams,$location) {

	$scope.affDetail = function() {
		angular.forEach($scope.datas, function(data,key) {
			if (data.id == $routeParams.refId) {
				$scope.details = data;
				var propNames = Object.getOwnPropertyNames(data);
				var myTab = {};
				var i = 0;
				angular.forEach(propNames, function(name) {
					myTab[i] = {propName : name, value : ""};
					i++;
				});
				var j = 0;
				angular.forEach(data, function(val) {
					myTab[j].value = val;
					j++;
				});
				$scope.tabFeatures = myTab;
			}
		});
	}

	$scope.retour = function() {
		$location.path('/');
	}


	$scope.initData = function() {
		$http.get("refs.json").success(function(data, status) {
			if (status == 200) {
				$scope.datas = data;
				$scope.affDetail();
			}else{
				$scope.datas = "";
			}
		});
	}
		

});

